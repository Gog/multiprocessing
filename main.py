# -*- coding: utf-8 -*-

import sys, getopt, os, csv
from zip_writer import create_archives
from zip_reader import read_archives
from datetime import datetime

def write_csv(path, rows, fieldnames):
    with open(path, mode='w', encoding='utf-8') as w_file:
        file_writer = csv.writer(w_file, delimiter=",", lineterminator="\r")
        file_writer.writerow(fieldnames)
        file_writer.writerows(rows)

def main(argv):

    help_text = 'main.py -o=<output-dir> -a=<archives-count> -x=<xml-files-count>'

    output_dir = './archives'
    archives_count = 50
    xml_files_count = 100

    try:
        opts, args = getopt.getopt(argv, 'o:a:x:', ['output-dir=', 'archives-count=', 'xml-files-count='])
    except getopt.GetoptError:
        print(help_text)
        sys.exit(2)
    for opt, arg in opts:
        if opt == '-h':
            print(help_text)
            sys.exit()
        elif opt in ('-o', '--output-dir'):
            output_dir = arg
        elif opt in ('-a', '--archives-count'):
            try:
                archives_count = int(arg)
            except:
                print('wrong input "' + arg + '" for archives count')
                sys.exit(2)
        elif opt in ('-x', '--xml-files-count'):
            try:
                xml_files_count = int(arg)
            except:
                print('wrong input "' + arg + '" for xml files count')
                sys.exit(2)

    print("""Writing archives ({}): {}""".format(archives_count, datetime.now()))
    create_archives(output_dir, archives_count, xml_files_count)
    print("""Archives saved: {}""".format(datetime.now()))

    print("""Reading archives: {}""".format(datetime.now()))
    levels, objects = read_archives(output_dir)
    print("""Archives read: {}""".format(datetime.now()))

    print("""Writing csv files: {}""".format(datetime.now()))
    write_csv(os.path.join(output_dir, 'levels.csv'), levels, ['id', 'level'])
    write_csv(os.path.join(output_dir, 'objects.csv'), objects, ['id', 'object_name'])
    print("""Done: {}""".format(datetime.now()))

    sys.exit()

if __name__ == '__main__':
    main(sys.argv[1:])