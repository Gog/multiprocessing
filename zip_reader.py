# -*- coding: utf-8 -*-

import glob, os, zipfile
import xml.etree.ElementTree as ET
from multiprocessing import Process, Queue, Manager
from io import BytesIO


def read_archived_files(zf, filename):
    with zf.open(filename) as file:
        xml_string = file.read().decode('utf-8')
        root = ET.fromstring(xml_string)

        id = None
        level = None

        for var in root.iter('var'):
            attrs = var.attrib
            name = attrs.get('name')

            if name == 'id':
                id = attrs.get('value')
            elif name == 'level':
                level = attrs.get('value')

        objects = [(id, object.attrib.get('name')) for object in root.iter('object')]

        return [id, level], objects


def file_reader_proc(queue, archives):
    for archive in archives:
        with open(archive, 'rb') as file:
            queue.put(file.read());
    queue.put('DONE')


def writer_proc(queue, levels, objects):
    while True:
        zipData = queue.get()
        if zipData == 'DONE':
            break

        zf = zipfile.ZipFile(BytesIO(zipData), "r")
        for filename in zf.namelist():
            if not os.path.isdir(filename):
                level, fileObjects = read_archived_files(zf, filename)
                levels.append(level)
                objects.extend(fileObjects)


def read_archives(output_dir):
    if not os.path.exists(output_dir):
        return None, None

    manager = Manager()
    levels = manager.list()
    objects = manager.list()

    archives = [file for file in glob.glob(output_dir + '/*.zip')]

    q = Queue()

    fr_proc = Process(target=file_reader_proc, args=(q, archives))
    w_proc = Process(target=writer_proc, args=(q, levels, objects))

    fr_proc.start()
    w_proc.start()

    q.close()
    q.join_thread()

    fr_proc.join()
    w_proc.join()

    return levels, objects
