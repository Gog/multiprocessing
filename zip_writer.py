# -*- coding: utf-8 -*-

import os, uuid, random, zipfile
import xml.etree.ElementTree as ET
from multiprocessing import Process, Queue
from io import BytesIO

def generateXML():
    root = ET.Element('root')

    ET.SubElement(root, 'var', {'name': 'id', 'value': str(uuid.uuid4())})
    ET.SubElement(root, 'var', {'name': 'level', 'value': str(random.randint(1, 100))})

    objects = ET.SubElement(root, 'objects')
    for i in range(1, 10):
        ET.SubElement(objects, 'object', {'name': str(uuid.uuid4())})

    return ET.tostring(root)


def create_zip(xml_files_count):
    in_memory = BytesIO()
    zf = zipfile.ZipFile(in_memory, 'w', zipfile.ZIP_DEFLATED)

    for i in range(xml_files_count):
        xml_data = generateXML()
        zf.writestr('data_' + str(i + 1) + '.xml', xml_data)

    zf.close()
    return in_memory.getvalue()


def file_writer_proc(queue):
    while True:
        data = queue.get()
        if data == 'DONE':
            break

        data, path = data

        with open(path, 'wb') as fw:
            fw.write(data)


def generator_proc(queue, output_dir, archives_count, xml_files_count):
    for i in range(archives_count):
        path = os.path.join(output_dir, 'archive_' + str(i + 1) + '.zip')
        data = create_zip(xml_files_count)
        queue.put((data, path))

    queue.put('DONE')


def create_archives(output_dir, archives_count, xml_files_count):
    q = Queue()

    if not os.path.exists(output_dir):
        os.makedirs(output_dir)

    fw_proc = Process(target=file_writer_proc, args=(q,))
    gen_proc = Process(target=generator_proc, args=(q, output_dir, archives_count, xml_files_count))

    fw_proc.start()
    gen_proc.start()

    q.close()
    q.join_thread()

    gen_proc.join()
    fw_proc.join()
